const { response } = require('express');
const Comment = require('../models/Comment');


/*Retornar todas as instâncias de um objeto no BD*/
const index = async(req,res) => {
    try {
        const comments = await Comment.findAll();
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json({err});
    }
};
/*Retornar informação de uma instância específica no BD*/
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        return res.status(200).json({comment});
    }catch(err){
        return res.status(500).json({err});
    }
};
/* Criar uma instância nova do Objeto no BD */
const create = async(req,res) => {  
    try{
          const comment = await Comment.create(req.body);
          return res.status(201).json({message: "Comentário adicionado com sucesso!", comment: comment});
      }catch(err){
          res.status(500).json({error: err});
      }
};
/*Editar uma instância específica no BD*/
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Comment.update(req.body, {where: {id: id}});
        if(updated) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(comment);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Comentário não encontrado");
    }
};
/*Deletar uma instância específica no BD*/
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Comment.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Comentário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Comentário não encontrado.");
    }
};
/*Retorna todas os comentários de um usuário no BD*/
const indexUser = async(req,res) => {
    try {
        const {UserId} = req.params;
        const comments = await Comment.findAll({where: {UserId: UserId}});
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json({err});
    }
}
const setUser = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        const user = await User.findByPk(req.body.id);
        await comment.setRole(user);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};
const removeUser = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        await comment.setRole(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
}


/*Export dos metodos*/
module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    indexUser,
    setUser,
    removeUser
};


