const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Comment = sequelize.define('Comment', {
    userName: {
        type:DataTypes.STRING,
        allowNull: false
    },
    title: {
        type:DataTypes.STRING
    },
    comment:{
        type:DataTypes.STRING
    },
    date: {
        type:DataTypes.DATE,
        allowNull: false
    }
});

Comment.associate = function(models) {
    Comment.belongsTo(models.User);
}

module.exports = Comment;